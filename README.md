# m3 - Muell Mess Maschine

Das M3 / Muell Mess Maschine ist ein innovatives Nachhaltigkeits Projekt des Hacklabors. 

Das M3 unterstuetzt den Recycling Process um die dabei frei werdenden Rohstoffe schneller dem Wertstoffkreislauf bzw der weiten Verwertung zuzufuehren. Mit dem Menschen in Mittelpunkt. 

Ein #Arduino Projekt auf Basis eines WeMOS D1 und Messungen mittels eines SparkFun_VL53L1X Time Flight Sensors. 